import java.util.*;

/**
 * A circular version of an array list.
 * <p>Operates as a standard java.util.ArrayList, but additions and removals
 * from the List's front occur in constant time, for its circular nature
 * eliminates the shifting required when adding or removing elements to the
 * front of the ArrayList.
 * </p>
 *
 * @author AMY PETRIS, cssc0210
 */
public final class CirArrayList<E> extends AbstractList<E> implements
        List<E>, RandomAccess {

    private E[] storage;
    private int head;
    private int tail;
    private static final int ARRAY_SIZE = 16;
    private int curSize;

    /**
     * Builds a new, empty CirArrayList.
     */
    public CirArrayList() {
        super();
        storage = (E[]) new Object[ARRAY_SIZE];
        head = 0;
        tail = 0;
        curSize = 0;
    }

    /**
     * Constructs a new CirArrayList containing all the items in the input
     * parameter.
     *
     * @param col the Collection from which to base
     */
    public CirArrayList(Collection<? extends E> col) {
        this();

        storage = (E[]) new Object[col.size()*2];
        for( E thing : col )
            add(size(),thing);
 /*       head = 0;
        curSize = col.size();
        tail = col.size()-1;
        int counter =0;
        for(E item : col){
            storage[counter] = item;
            counter++;
        }*/
    }


    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index (0 based) of the element to return.
     * @return element at the specified position in the list.
     * @throws IndexOutOfBoundsException if the index is out of range (index < 0
     *                                   || index >= size())
     */
    @Override
    public E get(int index) {
        checkIndexRange(index);
        return storage[convert(index)];
    }


    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     *
     * @param index index of the element to replace
     * @param value element to be stored at teh specified position
     * @return element previously at the specified position
     * @throws IndexOutOfBoundsException if index is out of the range (index < 0
     *                                   || index >= size())
     */
    @Override
    public E set(int index, E value) {
        checkIndexRange(index);
        E oldValue = storage[convert(index)];
        storage[convert(index)] = value;
        return oldValue;
    }

    /**
     * Inserts the specified element at the specified position in this list.
     * Shifts the element currently at that position (if any) and any
     * subsequent elements to the right (adds one to their indices).
     *
     * @param index index at which the specified element is to be inserted
     * @param value element to be inserted
     */
    @Override
    public void add(int index, E value) {
        checkIndexRangeAdd(index);
        int arrayIndex = convert(index);

       /*if(index == 0 && curSize != 0) {
            if (arrayIndex == 0) {
                head = storage.length - 1;
                storage[head] = value;
            }
            else {
                head--;
                storage[head] = value;
            }

        }*/

         if (index == size()) {
            storage[arrayIndex] = value;
            tail++;
        }

        else if (index < curSize) {
            for (int i = tail+1; i > arrayIndex; i--) {
                    storage[i] = storage[i-1];

            }
            storage[arrayIndex] = value;
            tail++;
        }
        curSize++;
        checkCapacity();
    }




    /**
     * Removes the element at the specified position in this list.  Shifts
     * any subsequent elements to the left (subtracts one from their indices).
     * Returns the element that was removed from the list.
     *
     * @param index index of element to remove
     * @return the element previously at the specified position.
     */
    @Override
    public E remove(int index) {
        checkIndexRange(index);
        int arrayIndex = convert(index);
        E removed = storage[arrayIndex];
        /*if(index == 0){
           head++;
        }*/
        if (index<curSize) {
            for (int i = arrayIndex; i < tail; i++) {
                storage[i] = storage[i + 1];
            }
            tail--;
        }
        curSize--;
        return removed;
    }

    /**
     * Reports the number of items in the List.
     *
     * @return the item count.
     */
    @Override
    public int size() {
        return curSize;
    }


    private void checkIndexRangeAdd(int index) {
        if (index < 0 || index > curSize) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void checkIndexRange(int index) {
        if (index < 0 || index >= curSize) {
            throw new IndexOutOfBoundsException();
        }
    }

    private int convert(int position) {
        int tmpArrayIndex;
        tmpArrayIndex = (head + position) % storage.length;
        return tmpArrayIndex;
    }

    private void checkCapacity() {
        double oldCapacity = storage.length;
        if (curSize / oldCapacity > 0.9) {
            growArray();
        }
        //if (curSize / oldCapacity < 0.25)
         //  collapseArray();
    }

    private void growArray() {
        int arraySize = size() * 2;
        E[] tmpstorage = (E[]) new Object[arraySize];
        int amt = storage.length - head;
        int index = tmpstorage.length - amt;
        if (head < tail) {
            for (int i = 0; i < curSize; i++) {
                tmpstorage[i] = get(i);
            }
            head = 0;
            tail = curSize;
            storage = tmpstorage;

        } /*else {
            for (int i = 0; i < curSize; i++) {
                if (i <= amt)
                    tmpstorage[i] = get(i);
                else
                    tmpstorage[i] = get(i);
            }
            head = 0;
            tail = curSize;
            storage = tmpstorage;
        }*/
        }



    private void collapseArray() {
        int arraySize = storage.length / 2;
        E[] tmpstorage = (E[]) new Object[arraySize];
        for (int i = 0; i < curSize; i++) {
            tmpstorage[i] = get(i);
        }
        storage = tmpstorage;

    }

}
