import java.util.ArrayList;
import java.util.Iterator;

public class BinarySearchTree<K extends Comparable<K>, V> implements MapADT<K, V> {

    private class Node {
        K key;
        V value;
        Node leftChild;
        Node rightChild;
        Node parent;
        boolean flag;

        public Node getParent() {
            return parent;
        }

        public void setParent(Node parent) {
            this.parent = parent;
        }

        public void setFlag(boolean flag){
            this.flag = flag;
        }

        public boolean getFlag() {
            return flag;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        public Node getLeftChild() {
            return leftChild;
        }

        public Node getRightChild() {
            return rightChild;
        }


        public K getKey() {

            return key;
        }
    }

    private Node current = null;
    private int size =0;

    /**
     * Returns true if the map has an object for the corresponding key.
     *
     * @param key object to search for
     * @return true if within map, false otherwise
     */
    public boolean contains(K key) {
        int flag = 0;
        while (current.getKey() != null) {
            if (key.compareTo(current.getKey()) == 0)
                flag = 1;
            else if (key.compareTo(current.getKey()) < 0) {
                current = current.getLeftChild();
            } else {
                current = current.getRightChild();
            }
        }
        if (flag == 1)
            return true;
        else
            return false;
    }

    /**
     * Adds the given key/value pair to the map.
     *
     * @param key   Key to add to the map
     * @param value Corresponding value to associate with the key
     * @return the previous value associated with this key or null if new
     */
    public V add(K key, V value) {

        if (current == null) {
            current.setValue(value);
            current.setKey(key);
            size++;
            return null;
        }

        while (current != null) {
            if (key.compareTo(current.getKey()) < 0) {
                current = current.getLeftChild();
            } else if (key.compareTo(current.getKey()) > 0) {
                current = current.getRightChild();
            }
        }

        if (!contains(key)) {
            current.setKey(key);
            current.setValue(value);
            size++;
            return null;
        } else {
            V old = current.getValue();
            current.setValue(value);
            return old;
        }

    }

    /**
     * Removes the key/value pair identified by the key parameter from the map.
     *
     * @param key item to remove
     * @return true if removed, false if not found or unable to remove
     */
    public boolean delete(K key) {
        size--;
        return true;
    }

    /**
     * Returns the value associated with the parameter key.
     *
     * @param key key to lookup in the map
     * @return Value associated with key or null if not found
     */
    public V getValue(K key) {
        while (current != null) {
            if (key.compareTo(current.getKey()) < 0) {
                current = current.getLeftChild();
            } else if (key.compareTo(current.getKey()) > 0) {
                current = current.getRightChild();
            }
        }

        return current.getValue();
    }

    /**
     * Returns the first key found with the parameter value.
     *
     * @param value value to locate
     * @return key of first item found with the matching value
     */
    public K getKey(V value) {
    }

    /**
     * Identifies the size of the map.
     *
     * @return Number of entries stored in the map.
     */
    public int size() {
        return size;
    }

    /**
     * Indicates if the map contains nothing.
     *
     * @return true if the map is empty, as the method cryptically indicates.
     */
    public boolean isEmpty() {
        return current == null;
    }

    /**
     * Resets the map to an empty state with no entries.
     */
    public void clear() {
        //use iterator to step through and call delete on all
    }

    /**
     * Provides a key iterator.
     *
     * @return Iterator over the keys (some data structures provided sorted)
     */
    public Iterator<K> keys() {
        ArrayList<K> arrlist = new ArrayList<K>();

        while(arrlist.size() <= size) {
            while (current.getLeftChild() != null) {
                parent = current;
                current = current.getLeftChild();
            }
            current.setFlag(true);
            arrList.add(current.getKey));
            if(current.getRightChild() != null) {
                parent = current
                current = current.getRightChild();
            }
            else if(parent.getFlag == false)
                current = parent
                arrList.add(current.getKey());
                if(right != null)
                    go right
                else
                    go up
             else
                 go u
        }


    }


    /**
     * Provides a value iterator. The values arrive corresponding to their
     * keys in the key order.
     *
     * @return Iterator over the values.
     */
    public Iterator<V> values() {
        //use iterator for key and ge
    }




}
