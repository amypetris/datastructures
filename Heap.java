
import java.util.*;

/**
 * A heap data structure implementing the priority queue interface.
 * <p>An efficient, array-based priority queue data structure.
 * </p>
 *
 * @author AMY PETRIS
 */

public final class Heap<E> extends AbstractQueue<E> implements Queue<E> {

    final Comparator<E> comp;
    final List<E> storage;
    static int counter = 1;
    static int count = 1;

    /***
     * The collection constructor generates a new heap from the existing
     * collection using the enclosed item's natural ordering. Thus, these
     * items must support the Comparable interface.
     * @param col
     */
    public Heap(Collection<? extends E> col) {
        this();
        for (E thing : col)
            offer(thing);
    }

    /***
     * The default constructor generates a new heap using the natural order
     * of the objects inside. Consequently, all objects placed in this
     * container must implement the Comparable interface.
     */
    public Heap() {
        comp = (Comparator<E>) Comparator.naturalOrder();
        storage = new java.util.ArrayList<>();
    }

    /***
     * Generates a new Heap from the provided collection using the specified
     * ordering. This allows the user to escape the natural ordering or
     * provide one in objects without.
     * @param col the collection to use
     * @param orderToUse the ordering to use when sorting the heap
     */
    public Heap(Collection<? extends E> col, Comparator<E> orderToUse) {
        this(orderToUse);
        for (E thing : col)
            offer(thing);
    }

    /***
     * Generates a new, empty heap using the Comparator object provided as
     * its parameter. Thus, items in this heap possess no interface
     * requirements.
     *
     * @param orderToUse
     */
    public Heap(Comparator<E> orderToUse) {
        comp = orderToUse;
        storage = new java.util.ArrayList<>();
    }

    /***
     * An IN-PLACE sort function using heapsort.
     *
     * @param data a list of data to sort
     */
    public static <T> void sort(List<T> data) {
        int endHeap = data.size() -1;
        int size = data.size();
        T temp;

        for (int i = 0; i < size; i++) {
            endHeap = data.size() - count;
            //swap
            temp = data.get(0);
            data.set(0, data.get(endHeap));
            data.set(endHeap, temp);

            count++;


            trickleDownSort(data, Comparator.naturalOrder(), 0, endHeap);
        }

    }

    /***
     * An IN-PLACE sort function using heapsort.
     *
     * @param data a list of data to sort
     * @param order the comparator object expressing the desired order
     */
    public static <T> void sort(List<T> data, Comparator<T> order) {
        heapify(data, order);

        int size = data.size();
        double endHeap;
        for (int i = 0; i < size; i++) {
            endHeap = data.size() - counter;

            //swap
            T temp = data.get(0);
            data.set(0, data.get((int)endHeap));
            data.set((int)endHeap, temp);

            counter++;

            trickleDownSort(data, order, 0, endHeap);
        }
    }

    /**
     * The iterator in this assignment provides the data to the user in heap
     * order. The lowest element will arrive first, but that is the only real
     * promise.
     *
     * @return an iterator to the heap
     */
    @Override
    public Iterator<E> iterator() {
        return storage.iterator();
    }

    /***
     * Provides the caller with the number of items currently inside the heap.
     * @return the number of items in the heap.
     */
    @Override
    public int size() {
        return storage.size();
    }

    /***
     * Inserts the specified element into this queue if it is possible to do
     * so immediately without violating capacity restrictions. Heaps
     * represent priority queues, so the first element in the queue must
     * represent the item with the lowest ordering (highest priority).
     *
     * @param e element to offer the queue
     * @return
     */
    @Override
    public boolean offer(E e) {
        storage.add(e);
        int index = storage.size() - 1;
        int parent = getParent(index);
        trickleUp(storage, comp, index, parent);
        return true;
    }

    /***
     * Retrieves and removes the head of this queue, or returns null if this
     * queue is empty.
     * @return the head of this queue, or null if this queue is empty
     */
    @Override
    public E poll() {
        E temp;

        if (size() == 0)
            return null;
        else {
            temp = storage.get(0);
            int endHeap = storage.size() - 1;
            storage.set(0, storage.get(endHeap));
            trickleDown(storage, comp, 0, endHeap);
            storage.remove(endHeap);
            return temp;
        }

    }

    /***
     * Retrieves, but does not remove, the head of this queue, or returns
     * null if this queue is empty.
     * @return the head of this queue, or null if this queue is empty
     */
    @Override
    public E peek() {
        if (storage.size() == 0)
            return null;
        else
            return storage.get(0);
    }

    private static int getParent(int index) {
        int parent = (index - 1) / 2;
        return parent;
    }

    private static int getLeftChild(int index) {
        int leftChild = (index * 2) + 1;
        return leftChild;
    }

    private static int getRightChild(int index) {
        int rightChild = (index * 2) + 2;
        return rightChild;
    }

    private static <E> void swap(List<E> heap, Comparator comp, int index, int parent) {
        E temp = heap.get(parent);
        heap.set(parent, heap.get(index));
        heap.set(index, temp);

    }


    private static <E> void trickleDown(List<E> heap, Comparator comp, int index, int endHeap) {
        endHeap--;
        E temp = heap.get(index);
        while (index < endHeap / 2) {
            int left = getLeftChild(index);
            int right = getRightChild(index);
            int compare;
            int smallerChild;
            compare = comp.compare(heap.get(left), heap.get(right));
            if (compare <= 0)
                smallerChild = left;
            else
                smallerChild = right;
            compare = comp.compare(heap.get(index), heap.get(smallerChild));
            if (compare < 0)
                break;
            else {
                swap(heap, comp, index, smallerChild);
                index = smallerChild;
            }
            heap.set(index, temp);
        }
    }

    private static <E> void trickleDownSort(List<E> heap, Comparator comp, int index, double endHeap) {
        endHeap--;
        E temp = heap.get(index);
        while (index < endHeap / 2) {
            int left = getLeftChild(index);
            int right = getRightChild(index);
            int compare;
            int largerChild;
            compare = comp.compare(heap.get(left), heap.get(right));
                if(right > endHeap)
                    largerChild  = left;
                 else if (compare > 0)
                    largerChild = left;
                else
                    largerChild = right;
                compare = comp.compare(heap.get(index), heap.get(largerChild));
                if (compare > 0)
                    break;
                else {
                    swap(heap, comp, index, largerChild);
                    index = largerChild;
                }
            }

        }

    private static <E> void heapify(List<E> heap, Comparator<E> comp) {
        for (int i = heap.size() - 1; i > 0; i--) {
            int index = i;
            int parent = getParent(index);
            int compare = comp.compare(heap.get(index), heap.get(parent));
            while (compare > 0) {
                swap(heap, comp, index, parent);
                index = parent;
                parent = getParent(index);
                heapify(heap, comp);
            }

        }
    }

    private static <E> void trickleUp(List<E> heap, Comparator comp, int index, int parent) {
        int compare = comp.compare(heap.get(index), heap.get(parent));
        if (index > 0 && compare < 0) {
            swap(heap, comp, index, parent);
            index = parent;
            parent = getParent(index);
            trickleUp(heap, comp, index, parent);
        }
    }

}
